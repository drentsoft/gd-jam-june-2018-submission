extends KinematicBody2D

""" 
Signals
"""
signal heated
signal died

"""
Sound Effects
"""
const SHIVERING = preload("res://assets/audio/sfx/freezing.wav")
const PANTING = preload("res://assets/audio/sfx/panting.wav")
var jumpSFX = []

"""
Temperature variables
"""
const EXPOSURE = 0
const HEAT_STROKE = 1

var temperature = 37.2

export (int) var minTemp = -10
export (float) var bodyTemp = 37.2 # The temperature the player always returns to in the shade or in the moonlight
export (int) var maxTemp = 60 # The temperature at which the player dies from overheating
export (float) var dissipationFactor = 0.3

export (float) var coldBreathOnTemp = 5
export (float) var sweatDripOnTemp = 40


export (float) var deathTimeCold = 4 # Recovery time before the player dies from cold
export (float) var deathTimeHot = 2 # Recovery time before the player dies from heat

"""
Movement variables
"""
export (float) var runSpeed = 400
export (float) var jumpSpeed = 500
export (float) var gravity = 2000

var velocity = Vector2()

var jumping = false
var doubleJump = true
var hasContact = false
var wallJump = false

export (bool) var immortality = false

var isDying = false
var isAlive = true

func _ready():
	loadSFX()
	$AnimatedSprite.animation = "run"
	$DyingTimer.connect("timeout", self, "dyingTimer_timeout")
	temperature = bodyTemp
	set_process(true)

func loadSFX():
	for i in range(3):
		jumpSFX.append(load("res://assets/audio/sfx/jump" + str(i+1) + ".wav"))

func _physics_process(delta):
	#dissipateHeat(delta)
	velocity.x = 0
	velocity.y += gravity * delta
	
	if isAlive:
		if is_on_floor():
			hasContact = true
			if jumping:
				jumping = false
				wallJump = false
				$AnimatedSprite.play("jump_land")
			else:
				$AnimatedSprite.play("run")
		else:
			if !$FloorCast.is_colliding():
				hasContact = false
				
		getInputs()
		checkTemperature()
	
	velocity = move_and_slide(velocity, Vector2(0,-1))
	updateUI()

func getInputs():
	if !wallJump:
		if Input.is_action_pressed("run_forward"):
			# Move right and temporarily increase the speed of the world
			velocity.x += runSpeed
			
		if Input.is_action_pressed("run_backward"):
			# If player isn't at the left of the screen, slow them down
			velocity.x -= runSpeed
		
	if Input.is_action_just_pressed("jump"):
		tryJump()
		
	if Input.is_action_just_pressed("crouch"):
		# Check for speed and slide the player if they're going fast enough
		pass

func updateUI():
	$DeathTimerLabel.text = str(int($DyingTimer.time_left))

func tryJump():
	var canJump = false
	if hasContact:
		doubleJump = true
		canJump = true
	elif doubleJump:
		canJump = true
		doubleJump = false
	if is_on_wall():
		canJump = true
		wallJump = true
	else:
		wallJump = false
	if canJump:
		if !doubleJump or wallJump:
			#print("Double jump!")
			velocity.y -= jumpSpeed * 0.5
		else:
			pass
			#print("Jumping!")
		velocity.y -= jumpSpeed
		"""if wallJump:
			print("Wall jump!")
			if $WallCastRight.is_colliding():
				# Apply left velocity
				velocity.x -= jumpSpeed * 100
			elif $WallCastLeft.is_colliding():
				# Apply right velocity
				velocity.x += jumpSpeed * 100"""
		$AnimatedSprite.play("jump")
		var i = rand_range(0, jumpSFX.size())
		$MovementSoundEffects.stream = jumpSFX[i]
		$MovementSoundEffects.play()
		jumping = true
		hasContact = false

func setRunFPS(speed):
	if speed > 10:
		$AnimatedSprite.frames.set_animation_speed("run", speed)

func checkTemperature():
	if temperature < coldBreathOnTemp:
		enableTemperatureSound(SHIVERING)
	elif temperature >= sweatDripOnTemp:
		enableTemperatureSound(PANTING)
	else:
		$TemperatureEffects.stop()		
	$coldBreath.emitting = (temperature <= coldBreathOnTemp)
	$FrozenOverlay.visible = (temperature <= 0)
	$sweatDrips.emitting = (temperature >= sweatDripOnTemp)
	#$sweatDrips.amount = 6 + temperature / sweatDripOnTemp
	
	if !immortality:
		if temperature >= maxTemp:
			if isAlive:
				startDying(HEAT_STROKE)
		elif temperature <= minTemp:
			if isAlive:
				startDying(EXPOSURE)
		else:
			stopDying()

func enableTemperatureSound(source):
	if $TemperatureEffects.stream != source:
		$TemperatureEffects.stream = source
	if !$TemperatureEffects.playing:
		$TemperatureEffects.play()
	

"""
Ideally I would like this to work like actual heat dissipation but not sure how to get a sense of 
the temperature of the surrounding world
"""
func dissipateHeat(delta, day, target, light=null):
	if day:
		if light:
			# Go to ambient (target) temp
			#temperature = lerp(temperature, target, light.dissipationRate * delta)
			print("Player is lit by " + light.name)
			temperature += clamp(target - temperature, -(light.dissipationRate * delta), light.dissipationRate * delta)
		else:
			# Go to body temp
			print("Player is not lit")
			temperature += clamp(bodyTemp - temperature, -(dissipationFactor * delta), dissipationFactor * delta)
	else:
		if light:
			# Go to body temp
			temperature += clamp(bodyTemp - temperature, -(light.dissipationRate * delta), light.dissipationRate * delta)
			#temperature = lerp(temperature, bodyTemp, light.dissipationRate * delta)
		else:
			# Go to ambient (target) temp
			temperature += clamp(target - temperature, -(dissipationFactor * delta), dissipationFactor * delta)
			#temperature = lerp(temperature, target, dissipationFactor * delta)

func addHeat(temp):
	temperature += temp
	#emit_signal("heated")

func dyingTimer_timeout():
	if isDying:
		if temperature <= minTemp or temperature >= maxTemp:
			die()

func startDying(type=EXPOSURE):
	isDying = true
	if $DyingTimer.is_stopped():
		if type == EXPOSURE:
			$DyingTimer.wait_time = deathTimeCold
		else:
			$DyingTimer.wait_time = deathTimeHot
		$DyingTimer.start()
		$DeathTimerLabel.visible = true

func stopDying():
	isDying = false
	$DyingTimer.stop()
	$DeathTimerLabel.visible = false

# Disable all signs of life once
func die():
	emit_signal("died")
	isAlive = false
	$coldBreath.emitting = false
	$sweatDrips.emitting = false
	$AnimatedSprite.play("jump_land")
	$Tween.interpolate_property(self, "rotation_degrees", 0, -90, 0.3, Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
	$Tween.start()