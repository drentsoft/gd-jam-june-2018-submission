extends Node

var text
var length = 0
var pos = 0

var centreX
var centreY

func _ready():
	text = $StoryLabel.text
	print("Got text from: " + $StoryLabel.name)
	print(text)
	$StoryLabel.text = ""
	print("Still there?")
	print(text)
	length = text.length()
	centreX = get_viewport().size.x / 2.0
	centreY = get_viewport().size.y / 2.0
	$Tween.interpolate_property($FallingTimmy, "rotation_degrees", 0, -90-(360*4), 2, Tween.TRANS_QUAD, Tween.EASE_OUT)
	$Tween.interpolate_property($FallingTimmy, "position", Vector2(centreX, -83), Vector2(centreX, centreY), 2, Tween.TRANS_QUAD, Tween.EASE_IN)
	$Tween.interpolate_callback($StoryLabel/TextTimer, 2.5, "start")
	$Tween.start()

func _on_TextTimer_timeout():
	print("Updating text.")
	if pos < length-1:
		print("Position updated to: " + str(pos))
		pos += 1
	else:
		print("Done!")
		$StoryLabel/TextTimer.stop()
	$StoryLabel.text = text.substr(0, pos)

func _on_StartButton_pressed():
	var time
	if $Tween.is_active():
		time = 1.0
	else:
		time = 0
	$Tween.interpolate_property($FallingTimmy, "rotation_degrees", $FallingTimmy.rotation_degrees, -90-(360*4), 2, Tween.TRANS_QUAD, Tween.EASE_OUT)
	$Tween.interpolate_property($FallingTimmy, "position", $FallingTimmy.position, Vector2(centreX, get_viewport().size.y), time, Tween.TRANS_QUAD, Tween.EASE_IN)
	$StartButton/ChangeSceneTimer.wait_time = time
	$StartButton/ChangeSceneTimer.start()

func _on_ChangeSceneTimer_timeout():
	get_tree().change_scene("res://scenes/Game.tscn")
