extends Label

const FROZEN = preload("res://assets/audio/sfx/spell3.wav")
const FIRE = preload("res://assets/audio/sfx/foom_0.wav")

func _ready():
	$coldSteam.emitting = false
	$smoke.emitting = false
	$fire.emitting = false
	pass

func _on_Player_died(player):
	if player.temperature <= player.minTemp:
		text = "You died of exposure!"
		set("custom_colors/font_color", Color(0.71, 0.9, 0.94))#set("custom_colors/font_color", Color(0.71, 0.8, 0.92)) # Blue
		centreEmitter($coldSteam)
		$coldSteam.emitting = true
		playSound(FROZEN)
		
	elif player.temperature >= player.maxTemp:
		text = "You died of heat stroke!"
		set("custom_colors/font_color", Color(0.9, 0.23, 0.23))#set("custom_colors/font_color", Color(0.9, 0.0, 0)) # Red
		burstIntoFlames()
		
	else: # Shouldn't happen but just in case
		if !player.get_node("VisibilityNotifier2D").is_on_screen():
			text = "You died the way you lived.\nOutside the box."
		else:
			text = "You haven't died yet, go you!"
		set("custom_colors/font_color", Color(0.9, 0.1, 0.96))
	visible = true

func burstIntoFlames():
	playSound(FIRE)
	centreEmitter($smoke)
	centreEmitter($fire)
	$smoke.emitting = true
	$fire.emitting = true

func playSound(source):
	$SFX.stream = source
	$SFX.play()

func centreEmitter(emitter):
	emitter.position.x = rect_size.x / 2.0