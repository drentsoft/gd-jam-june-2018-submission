extends Area2D

export (float) var minTemp = -100
export (float) var maxTemp = 100
export (float) var coolingRate = 5
export (float) var temperature = 50

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass

#func _process(delta):
	#temperature = lerp(temperature, minTemp, coolingRate * delta)

func addHeat(target, dissipation, delta):
	#temperature = lerp(temperature, target, dissipation * delta)
	temperature += clamp(target - temperature, -dissipation * delta, dissipation * delta)
	
"""func addHeat(target, delta):
	temperature = lerp(temperature, target, coolingRate * delta)"""

"""func addHeat(target, delta, day):
	if day:
		temperature = lerp(temperature, target, coolingRate * delta)
	else:
		temperature = lerp(temperature, minTemp, coolingRate * delta)"""