extends Sprite

const DAY = preload("res://assets/images/sun-2081062_640.png")
const NIGHT = preload("res://assets/images/moon-34603_640.png")

func setIcon(isDay):
	if isDay:
		texture = DAY
		flip_h = false
	else:
		texture = NIGHT
		flip_h = true