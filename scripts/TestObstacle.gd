extends Node2D

var childID = 0
var indexInParent = 0

const FONT = preload("res://resources/fonts/Monserrat-50.tres")

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass

func _process(delta):
	update()

func _draw():
	draw_line(Vector2(), Vector2(0, get_viewport().size.y), Color(1,0,0),10)
	draw_line(Vector2(get_viewport().size.x, 0), Vector2(get_viewport().size.x, get_viewport().size.y), Color(1,0.5,0.5),10)
	draw_string(FONT,get_viewport().size / 2.0, "Spawned: " + str(childID))
	draw_string(FONT,Vector2(get_viewport().size.x / 2.0, 100 + get_viewport().size.y / 2.0), "In parent: " + str(indexInParent))
