extends Light2D

"""
The script for the sun/moon. They impart temperature to all surfaces that can receive heat.
The moon should be cooler, imparting less heat
The sun should be much hotter, imparting a little too much heat
As the sun sets the colour of the light should change and the temperature should increase a bit.
As the sun rises the colour should change from red to white/blue and the temperature should decrease a bit.
As the moon rises the temperature should decrease
"""
export (Texture) var spriteImage
export (Vector2) var spriteScale = Vector2(1.0, 1.0)

export (float) var dissipationRate = 0.5

export (bool) var drawRay = false

var hitPos = Vector2()

func _ready():
	$Sprite.texture = spriteImage
	$Sprite.scale = spriteScale
	pass

func updateRaycast(player, delta, ambient, day):
	if $VisibilityNotifier2D.is_on_screen():
		var space_state = get_world_2d().direct_space_state
		var result = space_state.intersect_ray(global_position, player.global_position, [self], 1)
		if result:
			hitPos = result.position
			if result.collider.has_method("dissipateHeat"):
				result.collider.dissipateHeat(delta, day, ambient, self)
	update()

func _draw():
	if drawRay:
		var offsetHitPos = (hitPos - global_position).rotated(-global_rotation)
		draw_line(Vector2(), offsetHitPos, Color(1, 0, 0))
		draw_circle(offsetHitPos, 5, Color(1, 0, 0))