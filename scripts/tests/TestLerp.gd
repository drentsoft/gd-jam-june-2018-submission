extends Node2D

export (float) var targetTemp = 100
export (float) var dissipation = 0.1

export (float) var temp1 = 10
var temp2
var temp3
var temp4
var temp5

func _ready():
	temp2 = temp1
	temp3 = temp1
	temp4 = temp1
	temp5 = temp1
	$Label6.text = "Lerp"
	$Label7.text = "customLerp"
	$Label8.text = "addition"
	$Label9.text = "ClampedLerp1"
	$Label10.text = "ClampedLerp2"

func _process(delta):
	temp1 = lerp(temp1, targetTemp, dissipation * delta) # Actual lerp
	temp2 += (targetTemp - temp2) * dissipation * delta # Manual lerp?
	temp3 += dissipation# * delta * 10 #scaleFactor
	if temp3 > targetTemp:
		temp3 = targetTemp
	temp4 = temp4 + clamp(targetTemp - temp4, -dissipation * delta * 100, dissipation * delta * 100)#lerp(temp4, targetTemp, clamp((targetTemp - temp4), -dissipation * delta, dissipation * delta))
	temp5 = lerp(temp5, targetTemp, clamp(dissipation * delta, 0, 1))
	$Label1.text = str("%*.*f" % [3, 2,temp1])
	$Label2.text = str("%*.*f" % [3, 2,temp2])
	$Label3.text = str("%*.*f" % [3, 2,temp3])
	$Label4.text = str("%*.*f" % [3, 2,temp4])
	$Label5.text = str("%*.*f" % [3, 2,temp5])
	$Sprite1.position.y = 500 + temp1
	$Sprite2.position.y = 500 + temp2
	$Sprite3.position.y = 500 + temp3
	$Sprite4.position.y = 500 + temp4
	$Sprite5.position.y = 500 + temp5
	#update()

func _draw():
	draw_line(Vector2(0, 500), Vector2(1920, 500), Color(0.3, 0.2, 0.1), 2)
	draw_circle(Vector2(20, 500 + temp1), 5, Color(1, 0, 0))
	draw_circle(Vector2(40, 500 + temp2), 5, Color(0, 1, 0))
	draw_circle(Vector2(60, 500 + temp3), 5, Color(0, 0, 1))
	draw_circle(Vector2(80, 500 + temp4), 5, Color(1, 0, 1))
	draw_circle(Vector2(100, 500 + temp5), 5, Color(1, 1, 1))
	draw_line(Vector2(0, 500 + targetTemp), Vector2(1920, 500 + targetTemp), Color(0.3, 0.2, 0.1), 2)
