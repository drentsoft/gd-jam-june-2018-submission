extends Node

const LEVEL_SEGMENTS = [preload("res://scenes/levelSegments/StartSegment.tscn"),
						preload("res://scenes/levelSegments/EasyObstacle1.tscn"),
 						preload("res://scenes/levelSegments/EasyObstacle2.tscn"), 
 						preload("res://scenes/levelSegments/EasyObstacle3.tscn"), 
						preload("res://scenes/levelSegments/TestObstacle.tscn")]

var scrollSpeed = 200
var maxScrollSpeed = 1000
var scrollAccel = 0.5

export (float) var distScale = 1/68.2 # Worked out from player sprite that 1m is 68.2 pixels so set this as the scale for distance calcs
var distanceTravelled = 0

var maxSegments = 10
var totalSegmentsSpawned = 0

var activeLightSource
var timeOfDay = 12
var day = true
export (int) var morning = 6
export (int) var nightTime = 18
export (float) var timeSpeedFactor = 0.3

func _ready():
	randomize()
	spawnSegment(false)
	generateSegments()
	$Player.connect("died", $CenterContainer/DeathLabel, "_on_Player_died", [$Player])
	$Player.connect("died", self, "_on_Player_died")
	$Player/VisibilityNotifier2D.connect("screen_exited", self, "_on_Player_trapped")

# Check for the number of segments and add one at random (based on difficulty factor) to the end of the array
func generateSegments():
	if $levelSegments.get_child_count() < maxSegments:
		for i in range(maxSegments - $levelSegments.get_child_count()):
			spawnSegment()
			#print("Should be generating a level segment here")
			#print("Generated new segment at " + str(OS.get_ticks_msec()))
	pass

func spawnSegment(rand=true):
	totalSegmentsSpawned += 1
	var l
	if rand:
		l = rand_range(0,LEVEL_SEGMENTS.size())
	else:
		l = 0
	var inst = LEVEL_SEGMENTS[l].instance()
	inst.position.x = get_viewport().size.x * $levelSegments.get_child_count()
	inst.childID = totalSegmentsSpawned
	inst.indexInParent = $levelSegments.get_child_count()
	$levelSegments.add_child(inst)

func _process(delta):
	if Input.is_key_pressed(KEY_R):
		get_tree().reload_current_scene()
	$Player.dissipateHeat(delta, day, $Air.temperature)
	generateSegments() # Moved this here because queue_free is stopping them spawning as they leave.
	updateTime(delta)
	$Skylights.rotate(deg2rad((360 / 24) * timeSpeedFactor * delta))
	if $Skylights.rotation_degrees >= $Skylights.maxRotation:
		#print("Resetting lights!")
		$Skylights.rotation_degrees -= $Skylights.maxRotation
	moveWorld(delta)
	updateScroll(delta)
	updateUI()

func updateTime(delta):
	timeOfDay += timeSpeedFactor * delta
	if timeOfDay < 8:
		# Set the sun to an orange
		$Skylights/SunPivot/Sun.color = Color(0.95, 0.23, 0.23)
		pass
	elif timeOfDay > 15:
		# Set the sun to a red
		$Skylights/SunPivot/Sun.color = Color(0.91, 0.48, 0.1)
		pass
	else:
		# Set the sun to normal light yellowy white
		$Skylights/SunPivot/Sun.color = Color(0.87, 0.91, 0.53)
		pass
	if timeOfDay >= 24:
		timeOfDay = 0
	
	day = (timeOfDay >= morning and timeOfDay <= nightTime)	
	$TimeLabel/DayIndicator.setIcon(day)
	if day:
		$bg.modulate = Color(0.4, 0.71, 0.72)
		$Air.addHeat($Air.maxTemp, $Skylights/SunPivot/Sun.dissipationRate, delta)
	else:
		$bg.modulate = Color(0.12, 0.15, 0.26)
		$Air.addHeat($Air.minTemp, $Skylights/MoonPivot/Moon.dissipationRate, delta)

func updateUI():
	var hours = int(timeOfDay)
	$TimeLabel.text = "Time: " + str(hours)
	$SpeedLabel.text = "Speed: " + str("%10.2f" % scrollSpeed)
	$HeatLabel.text = "Temp: " + str("%*.*f" % [3, 2, $Player.temperature]) + "°C"
	$AmbientLabel.text = "Ambient: " + str("%*.*f" % [3, 2, $Air.temperature]) + "°C"
func moveWorld(delta):
	if $Player.isAlive:
		for s in $levelSegments.get_children():
			s.position.x -= scrollSpeed * delta
			if !s.get_node("VisibilityNotifier2D").is_on_screen() and s.position.x < 0:
				#print("Obstacle left screen, going to delete it now")
				s.queue_free()

func updateScroll(delta):
	if $Player.isAlive:
		distanceTravelled += scrollSpeed * delta * distScale
		#scrollSpeed = lerp(scrollSpeed, maxScrollSpeed, scrollAccel * delta)
		if scrollSpeed < maxScrollSpeed:
			scrollSpeed += scrollAccel * delta
		if scrollSpeed > maxScrollSpeed:
			scrollSpeed = maxScrollSpeed	

func _physics_process(delta):
	for l in $Skylights.get_children():
		if l.get_child(0) is preload("res://scripts/LightSource.gd"):
			l.get_child(0).updateRaycast($Player, delta, $Air.temperature, day)

func _on_Player_died():
	$CenterContainer2/DistanceLabel.text = "Distance: " + str("%10.0f" % distanceTravelled) + "m"
	$CenterContainer2/DistanceLabel.visible = true
	
func _on_Player_trapped():
	$Player.die()